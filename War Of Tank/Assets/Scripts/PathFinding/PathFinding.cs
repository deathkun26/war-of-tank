using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinding : MonoBehaviour
{
    public GridMovement grid;

    public void StartFindPath(float unitSizeRadius, Vector3 start, Vector3 target)
    {
        StartCoroutine(FindPath(unitSizeRadius, start, target));
    }

    IEnumerator FindPath(float unitSizeRadius, Vector3 start, Vector3 target)
    {
        float timeStart = Time.time;

        bool pathSuccess = false;

        Vector3[] wayPoints = null;

        Node startNode = grid.NodeFromWorldPosition(start);
        Node targetNode = grid.NodeFromWorldPosition(target);

        startNode.costSoFar = 0;
        targetNode.costSoFar = 0;

        Heap open = new Heap(grid.GetMaxSize());
        HashSet<Node> closed = new HashSet<Node>();

        open.Add(startNode);
        if (startNode != targetNode && startNode.isWalkable(unitSizeRadius, grid.unwalkableMask) && targetNode.isWalkable(unitSizeRadius, grid.unwalkableMask))
        {
            while (open.Count > 0)
            {
                Node currentNode = open.Pop();

                if (currentNode == targetNode)
                {
                    pathSuccess = true;
                    break;
                }

                foreach (Node neighbour in grid.GetNeighbour(currentNode))
                {
                    if (!neighbour.isWalkable(unitSizeRadius, grid.unwalkableMask)) continue;

                    float neighbourCost = currentNode.costSoFar + GetDistance(currentNode, neighbour);

                    float neighbourHeuristic;

                    if (closed.Contains(neighbour))
                    {
                        // * Skip if new cost is not better
                        if (neighbour.costSoFar <= neighbourCost) continue;

                        closed.Remove(neighbour);

                        neighbourHeuristic = neighbour.estimatedTotalCost - neighbour.costSoFar;
                    }
                    else if (open.Contains(neighbour))
                    {
                        // * Skip if new cost is not better
                        if (neighbour.costSoFar <= neighbourCost) continue;

                        neighbourHeuristic = neighbour.estimatedTotalCost - neighbour.costSoFar;
                    }
                    else
                    {
                        // * neighbour node is not visited
                        neighbourHeuristic = GetDistance(neighbour, targetNode);
                    }

                    neighbour.costSoFar = neighbourCost; // g(x)
                    neighbour.estimatedTotalCost = neighbourCost + neighbourHeuristic; // f(x) = g(x) + h(x)
                    neighbour.parent = currentNode;

                    if (!open.Contains(neighbour))
                        open.Add(neighbour);
                }

                closed.Add(currentNode);
            }

        }

        yield return null;

        if (pathSuccess)
        {
            wayPoints = RetracePath(startNode, targetNode);
        }

        float timeEnd = Time.time;

        Debug.Log("Finding time : " + TimeSpan.FromSeconds(timeEnd - timeStart).TotalMilliseconds +
                    " - Move to node : " + targetNode.gridX + " , " + targetNode.gridY);

        PathRequestManager.instance.FinishedProcessingPath(wayPoints, pathSuccess);
    }

    Vector3[] RetracePath(Node startNode, Node endNode)
    {
        List<Node> path = new List<Node>();
        Node currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        Vector3[] waypoints = SimplifyPath(path);
        Array.Reverse(waypoints);
        return waypoints;
    }

    Vector3[] SimplifyPath(List<Node> path)
    {
        List<Vector3> wayPoints = new List<Vector3>();
        Vector3 oldDirection = Vector3.zero;

        for (int i = 1; i < path.Count; i++)
        {
            Vector3 newDirection = new Vector2(path[i - 1].gridX - path[i].gridX, path[i - 1].gridY - path[i].gridY);
            if (newDirection != oldDirection)
            {
                wayPoints.Add(path[i - 1].worldPosition);
                oldDirection = newDirection;
            }
        }

        wayPoints.Add(path[path.Count - 1].worldPosition);

        return wayPoints.ToArray();
    }

    public float GetDistance(Node start, Node end)
    {
        return (start.worldPosition - end.worldPosition).sqrMagnitude;
    }
}
