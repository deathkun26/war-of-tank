using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public struct PathRequest
{
    public float unitSizeRadius;
    public Vector3 start;
    public Vector3 target;
    public Action<Vector3[], bool> callback;
    public PathRequest(float unitSizeRadius, Vector3 start, Vector3 target, Action<Vector3[], bool> callback)
    {
        this.unitSizeRadius = unitSizeRadius;
        this.start = start;
        this.target = target;
        this.callback = callback;
    }
}

public class PathRequestManager : MonoBehaviour
{
    public static PathRequestManager instance;
    public PathFinding pathFinding;
    Queue<PathRequest> pathRequestQueue = new Queue<PathRequest>();
    PathRequest currentPathRequest;
    bool isProcess;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void RequestPath(float unitSizeRadius, Vector3 start, Vector3 target, Action<Vector3[], bool> callback)
    {
        PathRequest newPathRequest = new PathRequest(unitSizeRadius, start, target, callback);
        pathRequestQueue.Enqueue(newPathRequest);
        TryProcessNext();
    }

    private void TryProcessNext()
    {
        if (!isProcess && pathRequestQueue.Count > 0)
        {
            currentPathRequest = pathRequestQueue.Dequeue();
            isProcess = true;

            pathFinding.StartFindPath(currentPathRequest.unitSizeRadius, currentPathRequest.start, currentPathRequest.target);
        }
    }

    public void FinishedProcessingPath(Vector3[] path, bool success)
    {
        currentPathRequest.callback(path, success);
        isProcess = false;
        TryProcessNext();
    }

}
