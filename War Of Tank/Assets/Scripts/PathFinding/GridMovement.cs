using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridMovement : MonoBehaviour
{
    public bool displayGridGizmos;
    public LayerMask unwalkableMask;
    public Vector2 gridWorldSize;
    public float nodeRadius;

    int gridSizeX, gridSizeY;
    float nodeDiameter;

    List<Vector2> offset = new List<Vector2>();

    public int GetMaxSize() => gridSizeX * gridSizeY;

    Node[,] grid;

    private void Start()
    {
        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);
        CreateGrid();
    }

    private void CreateGrid()
    {
        grid = new Node[gridSizeX, gridSizeY];
        Vector3 worldBottomLeft =
            transform.position -
            Vector3.right * gridWorldSize.x / 2 -
            Vector3.forward * gridWorldSize.y / 2;

        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                Vector3 worldPosition =
                    worldBottomLeft +
                    Vector3.right * (x * nodeDiameter + nodeRadius) +
                    Vector3.forward * (y * nodeDiameter + nodeRadius);
                // bool walkable = !Physics.CheckBox(
                //     worldPosition,
                //     new Vector3(nodeRadius, 2, nodeRadius),
                //     Quaternion.identity,
                //     unwalkableMask);
                grid[x, y] = new Node(true, worldPosition, x, y);
            }
        }

    }

    public Node NodeFromWorldPosition(Vector3 worldPosition)
    {
        float xPercent = (worldPosition.x + gridWorldSize.x / 2) / gridWorldSize.x;
        float yPercent = (worldPosition.z + gridWorldSize.y / 2) / gridWorldSize.y;

        xPercent = Mathf.Clamp01(xPercent);
        yPercent = Mathf.Clamp01(yPercent);

        int x = Mathf.RoundToInt((gridSizeX - 1) * xPercent);
        int y = Mathf.RoundToInt((gridSizeY - 1) * yPercent);

        return grid[x, y];
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(
            transform.position,
            new Vector3(gridWorldSize.x, 1, gridWorldSize.y));

        if (grid != null && displayGridGizmos)
        {
            foreach (Node node in grid)
            {
                Gizmos.color = node.isWalkable(nodeRadius, unwalkableMask) ? Color.white : Color.red;

                Gizmos.DrawCube(node.worldPosition, Vector3.one * (nodeDiameter - 0.1f));
            }
        }
    }

    public List<Node> GetNeighbour(Node node)
    {
        List<Node> neighbours = new List<Node>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                    continue;

                int neighbourX = node.gridX + x;
                int neighbourY = node.gridY + y;

                if (neighbourX >= 0 && neighbourX < gridSizeX
                    && neighbourY >= 0 && neighbourY < gridSizeY)
                {
                    neighbours.Add(grid[neighbourX, neighbourY]);
                }
            }
        }

        return neighbours;
    }

    public List<Node> GetTargetNodes(Vector3 startPosition, int count)
    {
        List<Node> targetNodes = new List<Node>();

        Node node = NodeFromWorldPosition(startPosition);

        //Debug.Log("Move group to : " + node.gridX + " - " + node.gridY);

        bool canGetMore = true;

        int xStart = 0;
        int xEnd = 0;
        int yStart = 0;
        int yEnd = 0;

        bool[,] check = new bool[gridSizeX, gridSizeY];

        if (node.isWalkable(nodeRadius, unwalkableMask))
            targetNodes.Add(node);

        while (targetNodes.Count != count)
        {
            canGetMore = false;

            if (xStart + node.gridX > 0)
            {
                canGetMore = true;
                xStart--;
            }

            if (xEnd + node.gridX < gridSizeX - 1)
            {
                canGetMore = true;
                xEnd++;
            }

            if (yStart + node.gridY > 0)
            {
                canGetMore = true;
                yStart--;
            }

            if (yEnd + node.gridY < gridSizeY - 1)
            {
                canGetMore = true;
                yEnd++;
            }

            if (!canGetMore) break;

            for (int x = xStart; x <= xEnd; x++)
            {
                for (int y = yStart; y <= yEnd; y++)
                {
                    if (x == 0 && y == 0) { continue; }

                    int neighbourX = node.gridX + x;
                    int neighbourY = node.gridY + y;

                    if (check[neighbourX, neighbourY]) { continue; }
                    else { check[neighbourX, neighbourY] = true; }

                    if (neighbourX >= 0 && neighbourX < gridSizeX
                        && neighbourY >= 0 && neighbourY < gridSizeY
                        && grid[neighbourX, neighbourY].isWalkable(nodeRadius, unwalkableMask))
                    {
                        targetNodes.Add(grid[neighbourX, neighbourY]);

                        if (targetNodes.Count == count) break;
                    }
                }
            }
        }

        return targetNodes;
    }
}
