using UnityEngine;
using System;
using System.Collections.Generic;


// * smallest Heap
public class Heap
{
    public int Count { get; private set; }

    Node[] items;

    public Heap(int capacity)
    {
        items = new Node[capacity];
        Count = 0;
    }

    public void Add(Node item)
    {
        items[Count] = item;
        ReHeapUp(Count);
        Count++;
    }

    public Node Pop()
    {
        Node firstItem = items[0];
        Count--;
        items[0] = items[Count];
        ReHeapDown(0);

        return firstItem;
    }

    public bool Contains(Node node)
    {
        foreach (Node item in items)
        {
            if (item == node) return true;
        }
        return false;
    }

    void ReHeapDown(int index)
    {
        int leftChild = index * 2 + 1;
        int rightChild = index * 2 + 2;
        if (leftChild < Count)
        {
            int smallChild = 0;
            if (rightChild < Count && items[leftChild].estimatedTotalCost > items[rightChild].estimatedTotalCost)
            {
                smallChild = rightChild;
            }
            else
                smallChild = leftChild;

            if (items[smallChild].estimatedTotalCost < items[index].estimatedTotalCost)
            {
                // Swap node
                Swap(smallChild, index);
                ReHeapDown(smallChild);
            }
        }
    }

    void ReHeapUp(int index)
    {
        if (index > 0)
        {
            int parent = (index - 1) / 2;
            if (items[parent].estimatedTotalCost > items[index].estimatedTotalCost)
            {
                Swap(parent, index);
                ReHeapUp(parent);
            }
        }
    }

    void Swap(int a, int b)
    {
        Node temp = items[a];
        items[a] = items[b];
        items[b] = temp;
    }
}

