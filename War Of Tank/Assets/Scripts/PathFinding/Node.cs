using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public Vector3 worldPosition;
    public bool walkable;

    public int gridX;
    public int gridY;

    public float costSoFar = 0;
    public float estimatedTotalCost = 0;

    public Node parent = null;

    public Node(bool walkable, Vector3 worldPosition, int gridX, int gridY)
    {
        this.worldPosition = worldPosition;
        this.walkable = walkable;
        this.gridX = gridX;
        this.gridY = gridY;
    }

    public bool isWalkable(float unitSizeRadius, LayerMask unwalkableMask)
    {
        return !Physics.CheckBox(worldPosition,
                     new Vector3(unitSizeRadius, 2, unitSizeRadius),
                     Quaternion.identity,
                     unwalkableMask);
    }
}
