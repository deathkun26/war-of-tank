using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamage : MonoBehaviour
{
    [SerializeField] private Health health;

    public void DealDamage(int damage)
    {
        health.DealDamage(damage);
    }
}
