using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class Health : NetworkBehaviour
{
    [SerializeField] private int maxHeath = 100;

    [SyncVar(hook = nameof(HandleHeathUpdated))]
    private int currentHealth;

    public event Action ServerOnDie;

    public event Action<int, int> ClientOnHealthUpdated;

    #region Server

    public override void OnStartServer()
    {
        currentHealth = maxHeath;
        UnitBase.ServerOnPlayerDie += ServerHandlePlayerDie;
    }

    public override void OnStopServer()
    {
        UnitBase.ServerOnPlayerDie -= ServerHandlePlayerDie;
    }

    [Server]
    public void DealDamage(int damage)
    {
        if (currentHealth == 0) return;

        currentHealth = Mathf.Max(currentHealth - damage, 0);

        if (currentHealth == 0)
        {
            ServerOnDie?.Invoke();
        }
    }

    [Server]
    private void ServerHandlePlayerDie(int connectionId)
    {
        if (connectionId == connectionToClient.connectionId)
        {
            DealDamage(currentHealth);
        }
    }

    #endregion


    #region Client

    private void HandleHeathUpdated(int oldHealth, int newHealth)
    {
        ClientOnHealthUpdated?.Invoke(newHealth, maxHeath);
    }

    #endregion
}
