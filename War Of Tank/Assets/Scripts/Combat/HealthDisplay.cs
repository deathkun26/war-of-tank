using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthDisplay : MonoBehaviour
{
    [SerializeField] private Health health;
    [SerializeField] private GameObject healthBarCanvas;
    [SerializeField] private Image healthBar;

    private Camera mainCamera;

    private void Awake()
    {
        health.ClientOnHealthUpdated += HandleHealthUpdated;
        mainCamera = Camera.main;
    }
    
    private void OnDestroy()
    {
        health.ClientOnHealthUpdated -= HandleHealthUpdated;
    }

    private void OnMouseEnter()
    {
        healthBarCanvas.SetActive(true);
    }

    private void OnMouseExit()
    {
        healthBarCanvas.SetActive(false);
    }

    private void HandleHealthUpdated(int currentHealth, int maxHealth)
    {
        healthBar.fillAmount = (float)currentHealth / maxHealth;
    }

}
