using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using TMPro;

public class LobbyMenu : MonoBehaviour
{
    [SerializeField] private GameObject lobbyUI;
    [SerializeField] private Button startGameButton;
    [SerializeField] private TMP_Text[] playerNameList;

    private void Start()
    {
        RTSNetworkManager.ClientOnConnected += HandleClientConnected;
        RTSPlayer.AuthorityOnPartyStateUpdated += AuthorityHandlePartyOwnerUpdated;
        RTSPlayer.ClientOnInfoUpdated += ClientHandleInfoUpdated;
    }

    private void OnDestroy()
    {
        RTSNetworkManager.ClientOnDisconnected -= HandleClientConnected;
        RTSPlayer.AuthorityOnPartyStateUpdated -= AuthorityHandlePartyOwnerUpdated;
        RTSPlayer.ClientOnInfoUpdated -= ClientHandleInfoUpdated;
    }

    private void AuthorityHandlePartyOwnerUpdated(bool newState)
    {
        startGameButton.gameObject.SetActive(newState);
    }

    private void ClientHandleInfoUpdated()
    {
        List<RTSPlayer> players = ((RTSNetworkManager)NetworkManager.singleton).Players;

        for (int i = 0; i < players.Count; i++)
        {
            playerNameList[i].text = players[i].GetDisplayName();
        }

        for (int i = players.Count; i < playerNameList.Length; i++)
        {
            playerNameList[i].text = "Waiting For Player...";
        }

        startGameButton.interactable = players.Count >= 2;

    }

    private void HandleClientConnected()
    {
        if (lobbyUI != null)
            lobbyUI.SetActive(true);
        else
            Debug.LogWarning("Missing LobbyUI");
    }

    public void StartGame()
    {
        NetworkClient.connection.identity.GetComponent<RTSPlayer>().CmdStartGame();
    }

    public void LeaveLobby()
    {
        if (NetworkServer.active && NetworkClient.isConnected)
        {
            Debug.Log("Host has left");
            NetworkManager.singleton.StopHost();
        }
        else
        {
            Debug.Log("Client has left");

            NetworkManager.singleton.StopClient();

            SceneManager.LoadScene(0);
        }
    }
}
