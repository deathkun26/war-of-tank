using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class TeamColorSetter : NetworkBehaviour
{
    [SerializeField] private Renderer[] listRenderer;

    [SyncVar(hook = nameof(HandleTeamMaterialIndexUpdated))]
    private int teamMaterialIndex = -1;

    #region Server

    public override void OnStartServer()
    {
        RTSPlayer player = connectionToClient.identity.GetComponent<RTSPlayer>();

        teamMaterialIndex = player.teamMaterialIndex;
    }

    #endregion

    #region Client

    private void HandleTeamMaterialIndexUpdated(int oldIndex, int newIndex)
    {
        Material newMaterial = ((RTSNetworkManager)RTSNetworkManager.singleton).GetMaterialList()[newIndex];
        foreach (Renderer renderer in listRenderer)
        {
            Material[] materials = renderer.materials;

            for (int i = 0; i < materials.Length; i++)
            {
                if (materials[i].name.StartsWith("team_default_mat"))
                {
                    materials[i] = newMaterial;
                }
            }

            renderer.materials = materials;
        }
    }

    #endregion
}
