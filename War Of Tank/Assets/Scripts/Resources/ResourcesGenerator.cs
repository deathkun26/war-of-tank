using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class ResourcesGenerator : NetworkBehaviour
{
    [SerializeField] private Health health;
    [SerializeField] private int resourcesPerInterval = 10;
    [SerializeField] private float interval = 2f;

    private RTSPlayer player;
    private WaitForSeconds wait;
    private Coroutine generateResources;

    public override void OnStartServer()
    {
        player = connectionToClient.identity.GetComponent<RTSPlayer>();

        health.ServerOnDie += ServerHandleDie;
        GameOverHandler.ServerOnGameOver += ServerHandleGameOver;

        wait = new WaitForSeconds(interval);

        generateResources = StartCoroutine(UpdateResources());
    }

    public override void OnStopServer()
    {
        health.ServerOnDie -= ServerHandleDie;
        GameOverHandler.ServerOnGameOver -= ServerHandleGameOver;
    }

    [Server]
    IEnumerator UpdateResources()
    {
        yield return wait;
        player.SetResources(player.GetResources() + resourcesPerInterval);

        generateResources = StartCoroutine(UpdateResources());
    }

    [Server]
    private void ServerHandleDie()
    {
        StopCoroutine(generateResources);

        NetworkServer.Destroy(gameObject);
    }

    [Server]
    private void ServerHandleGameOver()
    {
        StopCoroutine(generateResources);

        enabled = false;
    }
}
