using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceToCamera : MonoBehaviour
{
    private Transform mainCameraTransform;

    private void Start()
    {
        mainCameraTransform = Camera.main.transform;
    }

    private void LateUpdate()
    {
        if (gameObject.activeSelf)
        {
            transform.LookAt(transform.position - mainCameraTransform.rotation * Vector3.forward,
           mainCameraTransform.rotation * Vector3.up);

        }
    }
}
