using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class UnitProjectile : NetworkBehaviour
{
    [SerializeField] private Rigidbody rb;
    [SerializeField] private int damage = 20;
    [SerializeField] private float destroyTime = 5f;
    [SerializeField] private float launchForce = 10f;

    private void Start()
    {
        rb.velocity = transform.forward * launchForce;
    }

    public override void OnStartServer()
    {
        Invoke(nameof(DestroySelf), destroyTime);
    }

    [ServerCallback]
    private void OnTriggerEnter(Collider other)
    {
        NetworkIdentity networkIdentity = null;
        if (other.TryGetComponent<NetworkIdentity>(out networkIdentity)
            || other.transform.parent.TryGetComponent<NetworkIdentity>(out networkIdentity))
        {
            if (networkIdentity.connectionToClient.connectionId == connectionToClient.connectionId)
                return;
        }

        if (other.TryGetComponent<TakeDamage>(out TakeDamage takeDamage))
        {
            takeDamage.DealDamage(damage);
        }

        DestroySelf();
    }

    [Server]
    private void DestroySelf()
    {
        NetworkServer.Destroy(gameObject);
    }

    #region Client

    #endregion
}
