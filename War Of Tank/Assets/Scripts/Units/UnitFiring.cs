using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class UnitFiring : NetworkBehaviour
{
    [SerializeField] private Targeter targeter;
    [SerializeField] private GameObject projectilePrefab;
    [SerializeField] private Transform directionToTarget;
    [SerializeField] private Transform tankTransform;
    [SerializeField] private Transform projectileSpawnPosition;
    [SerializeField] private float fireRange = 10f;
    [SerializeField] private float fireRate = 1f;
    [SerializeField] private float rotationSpeed = 20f;

    private float lastFireTime;


    #region Server

    [ServerCallback]
    private void Update()
    {
        Targetable target = targeter.target;


        if (target == null)
        {
            directionToTarget.rotation = Quaternion.RotateTowards(directionToTarget.rotation, tankTransform.rotation, Time.deltaTime * rotationSpeed);

            return;
        }

        if (CanFireAtTarget())
        {
            Quaternion targetRotation = Quaternion.LookRotation(target.transform.position - directionToTarget.position);

            directionToTarget.rotation = Quaternion.RotateTowards(directionToTarget.rotation, targetRotation, Time.deltaTime * rotationSpeed);

            if (Time.time > (1 / fireRate) + lastFireTime && targetRotation == directionToTarget.rotation)
            {
                Quaternion projectileRotation =
                    Quaternion.LookRotation(target.GetAimAtPosition().position - projectileSpawnPosition.position);

                GameObject projectile =
                    Instantiate(projectilePrefab, projectileSpawnPosition.position, projectileRotation);


                lastFireTime = Time.time;

                NetworkServer.Spawn(projectile, connectionToClient);
            }
        }
        else
        {
            directionToTarget.rotation = Quaternion.RotateTowards(directionToTarget.rotation, tankTransform.rotation, Time.deltaTime * rotationSpeed);
        }

    }

    [Server]
    private bool CanFireAtTarget()
    {
        return (targeter.target.transform.position - transform.position).sqrMagnitude < fireRange * fireRange;
    }

    #endregion

}
