using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class UnitCommandGiver : MonoBehaviour
{
    [SerializeField] private GridMovement grid;
    [SerializeField] private UnitSelectionHandler unitSelectionHandler;
    [SerializeField] private LayerMask layerMask;

    private Camera mainCamera;

    private void Start()
    {
        mainCamera = Camera.main;

        GameOverHandler.ClientOnGameOver += ClientHandleGameOver;
    }

    private void OnDestroy()
    {
        GameOverHandler.ClientOnGameOver -= ClientHandleGameOver;
    }

    private void Update()
    {
        if (Mouse.current.rightButton.wasPressedThisFrame)
        {
            Ray ray = mainCamera.ScreenPointToRay(Mouse.current.position.ReadValue());

            if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, layerMask)) { return; }

            if (hit.collider.TryGetComponent<Targetable>(out Targetable target))
            {
                if (target.hasAuthority)
                {
                    TryMove(target.transform.position);
                }
                else
                {
                    TryTarget(target);
                }
            }
            else
            {
                TryMove(hit.point);
            }
        }
    }

    private void TryMove(Vector3 position)
    {
        List<Unit> selectedUnits = unitSelectionHandler.selectedUnits;

        if (selectedUnits.Count == 0) { return; }

        List<Node> targetNodes = grid.GetTargetNodes(position, selectedUnits.Count);

        if (targetNodes != null)
            for (int i = 0; i < selectedUnits.Count; i++)
            {
                selectedUnits[i].GetUnitMovement().Move(targetNodes[i].worldPosition);
            }

    }

    private void TryTarget(Targetable target)
    {
        foreach (Unit unit in unitSelectionHandler.selectedUnits)
        {
            unit.GetTargeter().CmdSetTarget(target.gameObject);
        }
    }

    private void ClientHandleGameOver(string winnerName)
    {
        enabled = false;
    }

    private List<Vector3> GetListPosition(int count, Node center)
    {
        List<Vector3> result = new List<Vector3>();

        int n = Mathf.FloorToInt(Mathf.Sqrt(count));

        for (int i = 0; i < n; i += 2)
        {

        }

        return result;
    }
}
