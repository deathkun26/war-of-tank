using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.Events;
using System;

public class Unit : NetworkBehaviour
{
    [SerializeField] private int price = 10;
    [SerializeField] private Health health;
    [SerializeField] private UnitMovement unitMovement;
    [SerializeField] private UnityEvent onSelected = null;
    [SerializeField] private UnityEvent onDeselected = null;
    [SerializeField] private Targeter targeter;

    public static event Action<Unit> ServerOnUnitSpawned;
    public static event Action<Unit> ServerOnUnitDespawned;

    public static event Action<Unit> AuthorityOnUnitSpawned;
    public static event Action<Unit> AuthorityOnUnitDespawned;

    public UnitMovement GetUnitMovement() => unitMovement;

    public Targeter GetTargeter() => targeter;

    public int GetPrice() => price;

    #region Sever

    public override void OnStartServer()
    {
        ServerOnUnitSpawned?.Invoke(this);

        health.ServerOnDie += ServerHandleDie;
    }

    public override void OnStopServer()
    {
        ServerOnUnitDespawned?.Invoke(this);

        health.ServerOnDie -= ServerHandleDie;
    }

    [Server]
    private void ServerHandleDie()
    {
        NetworkServer.Destroy(gameObject);
    }

    #endregion


    #region Client

    public override void OnStartAuthority()
    {
        AuthorityOnUnitSpawned?.Invoke(this);
    }

    public override void OnStopClient()
    {
        if (hasAuthority)
        {
            AuthorityOnUnitDespawned?.Invoke(this);
        }
    }

    [Client]
    public void Select()
    {
        if (hasAuthority) { onSelected?.Invoke(); }
    }

    [Client]
    public void Deselect()
    {
        if (hasAuthority) { onDeselected?.Invoke(); }
    }

    #endregion

}
