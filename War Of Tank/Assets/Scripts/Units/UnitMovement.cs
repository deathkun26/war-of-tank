using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Mirror;
using UnityEngine.InputSystem;

public class UnitMovement : NetworkBehaviour
{
    //[SerializeField] private NavMeshAgent agent;
    [SerializeField] private Targeter targeter;
    [SerializeField] private float linearSpeed;
    [SerializeField] private float angularSpeed;
    [SerializeField] private float unitSizeRadius;
    [SerializeField] private Transform tankTransform;
    //[SerializeField] private float chaseRange = 10f;
    //[SerializeField] private float intervalCheck = 1f;

    public Vector3 direction { get; private set; }

    int targetIndex;
    Vector3[] path;
    Vector3 currentWayPoint;

    //float lastCheck = 0f;

    #region Server

    // public override void OnStartServer()
    // {
    //     GameOverHandler.ServerOnGameOver += ServerHandleGameOver;
    // }

    // public override void OnStopServer()
    // {
    //     GameOverHandler.ServerOnGameOver -= ServerHandleGameOver;
    // }

    [ServerCallback]
    private void Update()
    {
        // Targetable target = targeter.target;

        // if (target != null)
        // {
        //     if (lastCheck > intervalCheck)
        //     {
        //         if ((target.transform.position - transform.position).sqrMagnitude > chaseRange * chaseRange)
        //         {
        //             //agent.SetDestination(target.transform.position);
        //             ClientMove(target.transform.position);
        //         }
        //         else
        //         {
        //             //agent.ResetPath();
        //             StopMove();
        //         }
        //         lastCheck = 0;
        //     }
        //     else
        //     {
        //         lastCheck += Time.deltaTime;
        //     }
        // }
        // else
        // {
        //     // if (agent.hasPath && agent.remainingDistance < agent.stoppingDistance)
        //     // {
        //     //     agent.ResetPath();
        //     // }
        // }


    }

    [Server]
    private void ServerHandleGameOver()
    {
        //agent.ResetPath();
        StopMove();
    }

    [Server]
    private void StopMove()
    {
        StopAllCoroutines();
        targetIndex = 0;
        path = null;
    }

    [Server]
    void RotateToPath()
    {
        // Rotate 

        StartCoroutine(RotateBody());
    }

    [Server]
    IEnumerator RotateBody()
    {
        currentWayPoint = path[targetIndex];

        direction = currentWayPoint - tankTransform.position;

        float angle = Mathf.Rad2Deg * Mathf.Atan2(direction.z, -direction.x) - 90;
        Quaternion newRotation = Quaternion.Euler(0, angle, 0);

        while (true)
        {
            if (Quaternion.Angle(tankTransform.rotation, newRotation) < 0.001f)
            {
                tankTransform.rotation = newRotation;
                yield return new WaitForEndOfFrame();

                StartCoroutine(FollowPath());
                yield break;
            }
            else
            {
                tankTransform.rotation = Quaternion.RotateTowards(tankTransform.rotation, newRotation, angularSpeed * Time.deltaTime);
                yield return null;
            }
        }

    }

    [Server]
    IEnumerator FollowPath()
    {
        while (true)
        {
            if (transform.position == currentWayPoint)
            {
                targetIndex++;
                if (targetIndex < path.Length)
                {
                    RotateToPath();
                    currentWayPoint = path[targetIndex];
                }

                yield break;
            }

            transform.position = Vector3.MoveTowards(transform.position, currentWayPoint, linearSpeed * Time.deltaTime);

            yield return null;
        }
    }

    [Command]
    public void CmdOnPathFound(Vector3[] newPath, bool success)
    {
        StopMove();
        if (success)
        {
            path = newPath;
            RotateToPath();
        }
    }

    #endregion


    #region Client

    [Client]
    public void Move(Vector3 targetPosition)
    {
        PathRequestManager.instance.RequestPath(unitSizeRadius, transform.position, targetPosition, CmdOnPathFound);
    }


    public void OnDrawGizmos()
    {
        if (path != null)
        {
            for (int i = targetIndex; i < path.Length; i++)
            {
                Gizmos.color = Color.black;
                Gizmos.DrawCube(path[i], Vector3.one);

                if (i == targetIndex)
                {
                    Gizmos.DrawLine(transform.position, path[i]);
                }
                else
                {
                    Gizmos.DrawLine(path[i - 1], path[i]);
                }
            }
        }
    }

    // [TargetRpc]
    // private void ClientMove(Vector3 targetPosition)
    // {
    //     Move(targetPosition);
    // }

    #endregion
}
